import { render, screen } from '@testing-library/react'
import App from './App'

describe('<App/>', () => {
  it('should render without crashinig', () => {
    render(<App />)
    expect(screen.findByText('Hello from Webpack!')).toBeInTheDocument()
  })
})
