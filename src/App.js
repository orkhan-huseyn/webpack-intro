import React from 'react'
import thanos from './assets/thanos.jpg'
import './style.css'

const App = () => {
  return (
    <div>
      <h1>Hello from Webpack!</h1>
      <img src={thanos} alt="" />
    </div>
  )
}

export default App
